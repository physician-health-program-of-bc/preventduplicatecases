# preventduplicatecases

In 5.23+ you can accidentally silently create duplicate cases by creating a case, clicking back, changing something (possibly under the impression you are updating the case you just created), then clicking save.

<b>This is only needed for 5.23.0 to 5.24.1 inclusive. The underlying cause was worked around in 5.24.2. However, it can still be used in 5.24.2+ to provide a nicer error message and cleaner recovery than a fatal error.</b>

The extension is licensed under [MIT](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM 5.23+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://lab.civicrm.org/extensions/preventduplicatecases/-/archive/master/preventduplicatecases-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/preventduplicatecases.git
cv en preventduplicatecases
```

## Usage

There's nothing you need to do after install.

## Known Issues

None
