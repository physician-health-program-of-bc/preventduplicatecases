<?php

require_once 'preventduplicatecases.civix.php';
use CRM_Preventduplicatecases_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function preventduplicatecases_civicrm_config(&$config) {
  _preventduplicatecases_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function preventduplicatecases_civicrm_xmlMenu(&$files) {
  _preventduplicatecases_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function preventduplicatecases_civicrm_install() {
  _preventduplicatecases_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function preventduplicatecases_civicrm_postInstall() {
  _preventduplicatecases_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function preventduplicatecases_civicrm_uninstall() {
  _preventduplicatecases_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function preventduplicatecases_civicrm_enable() {
  _preventduplicatecases_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function preventduplicatecases_civicrm_disable() {
  _preventduplicatecases_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function preventduplicatecases_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _preventduplicatecases_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function preventduplicatecases_civicrm_managed(&$entities) {
  _preventduplicatecases_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function preventduplicatecases_civicrm_caseTypes(&$caseTypes) {
  _preventduplicatecases_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function preventduplicatecases_civicrm_angularModules(&$angularModules) {
  _preventduplicatecases_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function preventduplicatecases_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _preventduplicatecases_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function preventduplicatecases_civicrm_entityTypes(&$entityTypes) {
  _preventduplicatecases_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function preventduplicatecases_civicrm_themes(&$themes) {
  _preventduplicatecases_civix_civicrm_themes($themes);
}

/**
 * Implements hook_civicrm_postProcess().
 * @param string $formName
 * @param CRM_Core_Form $form
 */
function preventduplicatecases_civicrm_postProcess($formName, $form) {
  if ($formName === 'CRM_Case_Form_Case') {
    // TODO: For better performance don't use session - use a SQL table and clean it up periodically.
    if (!empty($form->_submitValues['qfKey'])) {
      $session = CRM_Core_Session::singleton();
      $key = $form->_submitValues['qfKey'];
      $qfkeys = $session->get('preventduplicatecases') ?? array();
      if (isset($qfkeys[$key])) {
        // This shouldn't happen because it should be caught in validateForm(),
        // but check anyway.
        CRM_Core_Error::statusBounce(E::ts('This form has already been submitted.'));
      }
      else {
        // clear out stale ones while we're here
        $cutoff = date('YmdHis', strtotime('-1 hour'));
        foreach ($qfkeys as $k => $timestamp) {
          if ($timestamp < $cutoff) {
            unset($qfkeys[$k]);
          }
        }

        // add ours
        $qfkeys[$key] = date('YmdHis');
        $session->set('preventduplicatecases', $qfkeys);
      }
    }
  }
}

/**
 * Implements hook_civicrm_validateForm().
 * @param string $formName
 * @param array $fields
 * @param array $files
 * @param CRM_Core_Form $form
 * @param array $errors
 */
function preventduplicatecases_civicrm_validateForm($formName, $fields, $files, $form, &$errors) {
  if ($formName === 'CRM_Case_Form_Case') {
    if (!empty($form->_submitValues['qfKey'])) {
      $key = $form->_submitValues['qfKey'];
      $session = CRM_Core_Session::singleton();
      $qfkeys = $session->get('preventduplicatecases') ?? array();
      if (isset($qfkeys[$key])) {
        $errors['activity_subject'] = E::ts('This form has already been submitted.');
      }
    }
  }
}
